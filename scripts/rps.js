function playGame() {
	//Get the userchoice from the select-html-element with id=userSelect
	var userChoice = document.getElementById('userSelect').value;
	//Let the computer make a choice
	var computerChoice = Math.random();
	if (computerChoice < 0.34) {
		computerChoice = "rock";
	} else if (computerChoice <= 0.67) {
		computerChoice = "paper";
	} else {
		computerChoice = "scissors";
	}
	//Now, let's play!
	var result = compare(userChoice, computerChoice);
	//Manipulate the DOM to reflect the game's result'
	document.getElementById('userChoice').innerHTML = "You choose " + userChoice;
	document.getElementById('computerChoice').innerHTML = "The computer choose " + computerChoice;
	document.getElementById('compareResult').innerHTML = result;
}

function compare(choice1, choice2) {
	if (choice1 === choice2) {
		return 'The result is a tie!';
	}
	if (choice1 === 'rock') {
		if (choice2 === 'scissors') {
			return 'rock wins';
		} else {
			return 'paper wins';
		}
	}
	if (choice1 === 'paper') {
		if (choice2 === 'rock') {
			return 'paper wins';
		} else {
			return 'scissors wins';
		}
	}
	if (choice1 === 'scissors') {
		if (choice2 === 'paper') {
			return 'scissors wins';
		} else {
			return 'rock wins';
		}
	}
}